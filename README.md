<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

<!-- Update the below line with your artifact name -->
# Allocate One Number

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)

## Overview
This Pre-Built Transformation consists of a JST document that finds the first available number from an array of values (range of integers). The array is constructed by taking example values for the start and end values that span from 0 through 255, and the array is a collection of numbers that can be allocated, for example `[200,199,0,1,2,3,4,100,101,150]`. The output from this JST is an object containing the next available number in the array, e.g. `{"assigned": 5}`. In cases where there is no number available, then the output object would be `{"assigned": false}`. Another example for this can be `[149,19,0,2,3,4,100,101,150]`. The output from this JST will be `{"assigned": 1}`. Some potential use cases for this JST would be an array of port numbers, or IP addresses where only 1 quadrant (0-255) can be handled.

_Estimated Run Time_: Depends on the array size, but typically within seconds.

## Installation Prerequisites

Users must satisfy the following pre-requisites.

* Itential Automation Platform
  * `^2022.1`

## Features

The main benefits and features of the Pre-Built are outlined below.

* Finds the first available number within a range.

## Future Enhancements

The scope of any future enhancements to this Pre-Built will include:

* Increased efficiency in the algorithm by converting logic into a recursive call.

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section. 
* The Pre-Built can be installed from within **App-Admin_Essential**. Simply search for the name of your desired Pre-Built and click the install button.

## How to Run

To run this JST:

* Add the appropriate JST task to the workflow.
* Search for **allocateNew**.
* Assign values to all icoming variables: 
  - startRange: INT
  - endRange: INT
  - allocated: ARRAY of INT
