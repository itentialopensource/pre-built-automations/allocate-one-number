
## 0.0.11 [01-23-2024]

* deprecation notice

See merge request itentialopensource/pre-built-automations/allocate-one-number!11

---

## 0.0.10 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/allocate-one-number!10

---

## 0.0.9 [06-09-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/allocate-one-number!9

---

## 0.0.8 [12-03-2021]

* 21.2 certification

See merge request itentialopensource/pre-built-automations/allocate-one-number!8

---

## 0.0.7 [07-02-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/allocate-one-number!7

---

## 0.0.6 [06-11-2021]

* Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/allocate-one-number!6

---

## 0.0.5 [12-23-2020]

* changing input schema

See merge request itentialopensource/pre-built-automations/allocate-one-number!5

---

## 0.0.4 [12-23-2020]

* changing input schema

See merge request itentialopensource/pre-built-automations/allocate-one-number!5

---

## 0.0.3 [10-21-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/allocate-one-number!4

---

## 0.0.2 [10-06-2020]

* Update package.json and README.md

See merge request itentialopensource/pre-built-automations/allocate-one-number!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
